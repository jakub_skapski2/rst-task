#Napisz implementację wraz testami jednostkowymi dla następujących wymagań dotyczących przeliczania walut:
* na wejściu przekazujemy kwotę wraz z walutą, którą będziemy przeliczać, walutę do której będziemy przeliczać oraz strategię zaokrąglania
* współczynnik przeliczenia pobierany jest z zewnętrznej usługi
* usługa ta wymaga podania waluty z której bedziemy przeliczać, waluty do której będziemy przeliczać, oraz daty z której ma być zwrócony współczynnik
* współczynnik jest typu `float`
* współczynnik pobieramy zawsze dla aktualnego czasu
* chcemy mieć możliwość przeliczania tylko USD, EUR, PLN między sobą
* innych walut nie wspieramy
* możemy wybrać 2 strategie zaokrąglania kwoty
* zawsze w górę do dwóch miejsc po przecinku (domyślna)
* zawsze w dół do dwóch miejsc po przecinku
