<?php
namespace App\Classes;

use App\Enums\CurrencyEnum;
use App\Exceptions\CurrencyException;
use App\Interfaces\CurrencyConverterInterface;

class Currency
{

    private $code, $amount, $roundMethod;

    public function __construct($code, $amount = null, $roundMethod = null)
    {
        $this->setCode($code);
        $this->roundMethod = $roundMethod;
        
        if ($amount !== null)
            $this->setAmount($amount);
    }

    public function setAmount($amount)
    {
        $this->validateAmount($amount);
        $this->amount = Rounder::round(floatval($amount), $this->roundMethod, 2);
    }

    /**
     * 
     * @return Currency
     */
    public function convertTo($code, $date = null)
    {
        return $this->convert($this->getDefaultConverter(), $code, $date);
    }
    
    public function getCode() {
        return $this->code;
    }
    
    public function getAmount() {
        return $this->amount;
    }
    
    public function convert(CurrencyConverterInterface $converter, $code, $date = null)
    {
        $this->validateCode($code);
        
        $to = new Currency($code, null, $this->roundMethod);

        $converter = $converter->from($this)->to($to);
        
        if($date !== null)
            $converter->setDate($date);
        
        $converter->convert();
        
        if($converter->hasErrors())
            throw new CurrencyException($converter->getErrorMessage());
        
        return $converter->getTo();
    }

    private function setCode($code)
    {
        $code = strtoupper($code);

        $this->validateCode($code);
        $this->code = $code;
    }

    private function validateCode($code)
    {
        $availableCurrencies = CurrencyEnum::getAll();
        if (!array_key_exists($code, $availableCurrencies))
            throw new CurrencyException('Code not found');
    }

    private function validateAmount($amount)
    {
        if (!is_numeric($amount))
            throw new CurrencyException('Amount must be numeric');

        $float = floatval($amount);

        if ($float <= 0)
            throw new CurrencyException('Amount must be greater than 0');
    }

    private function getDefaultConverter()
    {
        return new CurrencyConverter;
    }
}
