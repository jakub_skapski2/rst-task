<?php
namespace App\Classes;

use App\Interfaces\CurrencyConverterInterface;
use App\Exceptions\CurrencyConverterException;
use App\Interfaces\CurrencyApiInterface;

class CurrencyConverter implements CurrencyConverterInterface
{

    private $from, $to;
    private $date, $rate, $result, $error;

    public function __construct()
    {
        $this->date = date('Y-m-d');
    }

    public function from(Currency $from)
    {
        if ($from->getAmount() === null)
            throw new CurrencyConverterException('From currency must contain amount');

        $this->from = $from;

        return $this;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function to(Currency $to)
    {
        $this->to = $to;

        return $this;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function setDate($date)
    {
        if (!$this->isValidDateFormat($date))
            throw new CurrencyConverterException('Invalid date format');

        $this->date = $date;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function convert(CurrencyApiInterface $apiConverter = null)
    {
        if(!$this->hasRequiredAttributesToConvert())
            throw new CurrencyConverterException('Required attributes are not set');
        
        if (!$apiConverter)
            $apiConverter = $this->getDefaultApi();

        try {
            $this->rate = $apiConverter->getRateByDate(
                $this->from->getCode(), $this->to->getCode(), $this->date
            );

            $this->result = $this->from->getAmount() * $this->rate;
            
            $this->to->setAmount($this->result);
        } catch (\Exception $ex) {
            $this->error = $ex->getMessage();
        }
    }

    public function hasErrors()
    {
        if (!$this->error)
            return false;

        return true;
    }

    public function getErrorMessage()
    {
        return $this->error;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function getRate()
    {
        return $this->rate;
    }

    public function getDefaultApi()
    {
        return new \App\Webservices\CurrencyConverter\ExchangeRate;
    }

    private function isValidDateFormat($date)
    {
        if (\Carbon\Carbon::hasFormat($date, 'Y-m-d'))
            return true;
        return false;
    }
    
    private function hasRequiredAttributesToConvert()
    {
        if(!$this->getTo() || !$this->getFrom() || !$this->getDate()) 
            return false;
        return true;
    }
}
