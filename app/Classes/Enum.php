<?php
namespace App\Classes;

abstract class Enum
{

    public static function getAll() 
    {
        return self::toArray();
    }

    public static function toSelect() 
    {
        return [null => 'Please select'] + self::reverse(self::toArray());
    }

    public static function toArray()
    {
        return self::getReflectionClass()->getConstants();
    }
    
    public static function reverse($array)
    {
        return array_combine(array_values($array), array_keys($array));
    }
    
    public static function moveElementToBeginning(array &$array, $value)
    {
        $key = array_search($value, $array);
        
        if($key === false)
            throw new \ErrorException('Key not found');
        
        unset($array[$key]);
        
        $array = array_merge([$key => $value], $array);
    }

    protected static function getReflectionClass()
    {
        return new \ReflectionClass(get_called_class());
    }
}
