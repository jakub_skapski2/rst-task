<?php

namespace App\Classes;

use App\Enums\RoundMethodEnum;
use App\Exceptions\RounderException;

class Rounder
{
    public static function round($value, $method = null, $precision = 0)
    {
        if(!is_numeric($value))
            throw new RounderException('Value must be numeric!');
        
        if(!is_int($precision))
            throw new RounderException('Precision must be numeric!');
        
        //FIX for round methods to work as round() precision argument

        $value = floatval($value);
        $method = intval($method);
        
        switch($method)
        {
            case RoundMethodEnum::ALWAYS_UP:
                return self::roundUp($value, $precision);
            case RoundMethodEnum::ALWAYS_DOWN:
                return self::roundDown($value, $precision);
            default:
                return self::round($value, RoundMethodEnum::$defaultMethod, $precision);
        }
        
        throw new \ErrorException('Undefined error');
    }
    
    private static function roundUp($value, $precision = 0)
    {
        $fig = pow(10, $precision);
        
        return (ceil($value * $fig) / $fig);
    }

    private static function roundDown($value, $precision = 0)
    {
        $fig = pow(10, $precision);     
        
        return floor($value * $fig) / $fig;
    }
}