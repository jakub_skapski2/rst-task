<?php
namespace App\Classes;

class Uri
{

    private $uri;

    const defaultScheme = 'https';

    public function __construct($uri)
    {
        $this->uri = parse_url($uri);
        if(isset($this->uri['path']))
            $this->setPath($this->uri['path']);
    }

    public function toString()
    {
        $str = $this->getScheme() .
            $this->getSchemeSeparator() .
            $this->getHost() .
            $this->getPath() .
            $this->getQuery();

        return $str;
    }

    public function getScheme()
    {
        return $this->uri['scheme'] ?? self::defaultScheme;
    }

    public function getHost()
    {
        return $this->uri['host'];
    }

    public function getPath()
    {
        if (isset($this->uri['path']) && $this->uri['path'] !== '/')
            return $this->uri['path'];

        return '/';
    }

    public function setPath($path)
    {
        $this->uri['path'] = $this->parsePath($path);
        
        return $this;
    }

    public function getQuery()
    {
        if (isset($this->uri['query']))
            return '?' . $this->uri['query'];

        return null;
    }

    private function getSchemeSeparator()
    {
        return '://';
    }

    private function parsePath($string)
    {
        $string = '/' . $string;
        
        $string = preg_replace('/[^a-z\/!]/', '', strtolower($string));
        if (substr($string, 0, 1) === '/') {
            $string = substr($string, 1);
        }

        if (substr($string, -1) !== '/') {
            $string = $string . '/';
        }
        
        return $string;
    }
}
