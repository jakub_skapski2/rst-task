<?php
namespace App\Enums;

use App\Classes\Enum;

class CurrencyEnum extends Enum
{

    const USD = 'USD';
    const EUR = 'EUR';
    const PLN = 'PLN';
}
