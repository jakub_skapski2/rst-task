<?php
namespace App\Enums;

use App\Classes\Enum;

class RoundMethodEnum extends Enum
{
    public static $defaultMethod = self::ALWAYS_UP;
    
    const ALWAYS_UP = 1;
    const ALWAYS_DOWN = 2;
    
    public static function toSelect()
    {
        $array = parent::toArray();
        self::moveElementToBeginning($array, self::$defaultMethod);
        
        return self::reverse($array);
    }
    
    
}
