<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CurrencyConverterController extends Controller
{

    public function show(Request $request)
    {
        return $this->getView();
    }

    public function convert(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'from' => 'required',
                'to' => 'required',
                'round' => 'required',
                'amount' => 'required'
        ]);
        
        if ($validator->errors()->any())
        {
            return $this->getView()->with('errors', $validator->errors());
        }
        
        try
        {
            $from = new \App\Classes\Currency($request->get('from'), $request->get('amount'), $request->get('round'));
            $converted = $from->convertTo($request->get('to'));
        } catch (\Exception $ex) {
            $errors = new \Illuminate\Support\MessageBag;
            $errors->add('Currency error', $ex->getMessage());
            
            return $this->getView()->with('errors', $errors);
        }
        
        $result = $from->getAmount() . ' ' . $from->getCode() . ' = ' . $converted->getAmount() . ' ' . $converted->getCode();
        
        return $this->getView()->with('result', $result);
    }

    private function getView()
    {
        $currencies = \App\Enums\CurrencyEnum::toSelect();
        $roundMethods = \App\Enums\RoundMethodEnum::toSelect();

        return view('show', compact('currencies', 'roundMethods'));
    }
}
