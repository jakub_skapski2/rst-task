<?php
namespace App\Interfaces;

interface CurrencyApiInterface
{
    public function getRateByDate($from, $to, $date);
}
