<?php
namespace App\Interfaces;

use App\Classes\Currency;

interface CurrencyConverterInterface
{
    public function from(Currency $from);
    public function to(Currency $to);
    public function setDate($date);
    public function convert();
}
