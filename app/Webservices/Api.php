<?php
namespace App\Webservices;

use GuzzleHttp\Client;
use App\Classes\Uri;

abstract class Api
{
    /**
     *  @var Client $client
     */
    protected $client;

    abstract protected function getBaseUri();

    public function __construct()
    {
        $parsedBaseUri = $this->parseUri($this->getBaseUri());
        
        $this->client = new Client([
            'base_uri' => $parsedBaseUri->toString()
        ]);
    }
    
    private function parseUri($uriString) {
        $uri = new Uri($uriString);
        
        return $uri;
    }
}
