<?php
namespace App\Webservices\CurrencyConverter;

use App\Webservices\Api;
use App\Exceptions\RequestException;
use App\Interfaces\CurrencyApiInterface;

class ExchangeRate extends Api implements CurrencyApiInterface
{

    private $from, $to, $date, $response;

    public function getRateByDate($from, $to, $date)
    {
        $from = strtoupper($from);
        $this->from = $from;

        $to = strtoupper($to);
        $this->to = $to;

        $this->date = $date;
        
        $request = $this->client->request('GET', $date, ['query' => ['symbols' => $this->to, 'base' => $this->from]]);
        $this->response = json_decode($request->getBody());
        
        if (!$this->response instanceof \stdClass)
            throw new RequestException('No data received');

        $this->validateResponse();

        return $this->response->rates->$to;
    }

    protected function getBaseUri()
    {
        return 'https://api.exchangeratesapi.io/';
    }

    private function validateResponse()
    {
        if ($this->response->date !== $this->date)
            throw new RequestException('Data not available on this date');

        if ($this->response->base !== $this->from)
            throw new RequestException('Error has occured while retrieving rate #1');

        $to = $this->to;
        if ($this->response->rates->$to === null)
            throw new RequestException('Error has occured while retrieving rate #2');
    }
}
