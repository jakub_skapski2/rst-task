<html>
    <body>
        @if (isset($errors) && $errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(isset($result))
            Wynik: <h2>{{ $result }}</h2>
        @endif
        <form action="/" method="POST">
            <label>From</label>
            <select name="from">
                @foreach($currencies as $key => $currency)
                    <option value="{{ $key }}">{{ $currency }}</option>
                @endforeach
            </select>
            
            <label>To</label>
            <select name="to">
                @foreach($currencies as $key => $currency)
                    <option value="{{ $key }}">{{ $currency }}</option>
                @endforeach
            </select>
            
            <label>Round method</label>
            <select name="round">
                @foreach($roundMethods as $key => $method)
                    <option value="{{ $key }}">{{ $method }}</option>
                @endforeach
            </select>
            
            <label>Amount</label>
            <input type="text" name="amount" />
            
            <button type="submit">Submit</button>
        </form>
    </body>
</html>