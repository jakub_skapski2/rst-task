<?php

use App\Classes\CurrencyConverter;

class CurrencyConverterTest extends TestCase
{

    private $converter;

    public function setUp()
    {
        parent::setUp();
        $this->converter = new CurrencyConverter;
    }

    public function testIfCreatesWithTodaysDate()
    {
        $this->assertEquals(date('Y-m-d'), $this->converter->getDate());
    }

    public function testFrom()
    {
        $mock = Mockery::mock(\App\Classes\Currency::class);
        $mock->shouldReceive('getAmount')
            ->twice()
            ->andReturn(12.50);

        $this->assertInstanceOf(\App\Classes\CurrencyConverter::class, $this->converter->from($mock));
        $this->assertInstanceOf(\App\Classes\Currency::class, $this->converter->getFrom());
        $this->assertEquals(12.50, $this->converter->getFrom()->getAmount());
    }

    public function testTo()
    {
        $mock = Mockery::mock(\App\Classes\Currency::class);
        $mock->shouldReceive('getAmount')
            ->once()
            ->andReturn(12.50);

        $this->assertInstanceOf(\App\Classes\CurrencyConverter::class, $this->converter->to($mock));
        $this->assertInstanceOf(\App\Classes\Currency::class, $this->converter->getTo());
        $this->assertEquals(12.50, $this->converter->getTo()->getAmount());
    }

    public function testSetDateSuccesful()
    {
        $this->converter->setDate(date('Y-m-d'));
        $this->assertEquals(date('Y-m-d'), $this->converter->getDate());

        $this->converter->setDate('2015-12-03');
        $this->assertEquals('2015-12-03', $this->converter->getDate());
    }

    /**
     * @expectedException \App\Exceptions\CurrencyConverterException
     */
    public function testSetDateToThrowExceptionWithDate()
    {
        $this->converter->setDate(date('d-m-Y'));
    }

    /**
     * @expectedException \App\Exceptions\CurrencyConverterException
     */
    public function testSetDateToThrowExceptionWithString()
    {
        $this->converter->setDate('asd');
    }

    public function testConvertIsSuccessful()
    {
        $apiMock = Mockery::mock(App\Interfaces\CurrencyApiInterface::class);
        $apiMock->shouldReceive('getRateByDate')->once()->andReturn(0.4);

        $currencyFromMock = Mockery::mock(\App\Classes\Currency::class);
        $currencyFromMock->shouldReceive('getAmount')
            ->twice()
            ->andReturn(12.50);
        $currencyFromMock->shouldReceive('getCode')
            ->once()
            ->andReturn('PLN');

        $currencyToMock = Mockery::mock(\App\Classes\Currency::class);
        $currencyToMock->shouldReceive('getCode')
            ->twice()
            ->andReturn('USD');
        $currencyToMock->shouldReceive('setAmount')
            ->once();
        $currencyToMock->shouldReceive('getAmount')
            ->once()
            ->andReturn(12.50 * 0.4);

        $this->converter->from($currencyFromMock)->to($currencyToMock)->convert($apiMock);

        $this->assertEquals(0.4, $this->converter->getRate());
        $this->assertEquals(0.4 * 12.50, $this->converter->getResult());
        $this->assertEquals(0.4 * 12.50, $this->converter->getTo()->getAmount());
        $this->assertEquals('USD', $this->converter->getTo()->getCode());
        $this->assertEquals(false, $this->converter->hasErrors());
    }

    /**
     * @expectedException \App\Exceptions\CurrencyConverterException
     */
    public function testConvertThrowsExceptionWithFrom()
    {
        $apiMock = Mockery::mock(App\Interfaces\CurrencyApiInterface::class);
        $apiMock->shouldNotReceive('getRateByDate');

        $currency = Mockery::mock(\App\Classes\Currency::class);
        $currency->shouldReceive('getAmount')
            ->once()
            ->andReturn(12.50);

        $this->converter->from($currency)->convert($apiMock);
    }

    /**
     * @expectedException \App\Exceptions\CurrencyConverterException
     */
    public function testConvertThrowsExceptionWithTo()
    {
        $apiMock = Mockery::mock(App\Interfaces\CurrencyApiInterface::class);
        $apiMock->shouldNotReceive('getRateByDate');

        $currency = Mockery::mock(\App\Classes\Currency::class);

        $this->converter->to($currency)->convert($apiMock);
    }

    /**
     * @expectedException \App\Exceptions\CurrencyConverterException
     */
    public function testConvertThrowsExceptionWithoutArguments()
    {
        $apiMock = Mockery::mock(App\Interfaces\CurrencyApiInterface::class);
        $apiMock->shouldNotReceive('getRateByDate');

        $this->converter->convert($apiMock);
    }
    
    public function testIfReturnsDefaultApi()
    {
        $this->assertInstanceOf(App\Interfaces\CurrencyApiInterface::class, $this->converter->getDefaultApi());
    }
}
