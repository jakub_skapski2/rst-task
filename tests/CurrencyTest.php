<?php

use App\Classes\Currency;

class CurrencyTest extends TestCase
{

    public function testCreationSuccessful()
    {
        $currency = new Currency('PLN', 12.50);

        $this->assertEquals(12.50, $currency->getAmount());
        $this->assertEquals('PLN', $currency->getCode());
    }

    public function testCreationSuccessfulWithoutAmount()
    {
        $currency = new Currency('pln');

        $this->assertEquals(null, $currency->getAmount());
        $this->assertEquals('PLN', $currency->getCode());
    }

    public function testSetAmountNotNull()
    {
        $currency = new Currency('PLN', 12.50);
        $currency->setAmount(20.24);

        $this->assertEquals(20.24, $currency->getAmount());
    }

    public function testSetAmountNull()
    {
        $currency = new Currency('PLN');
        $currency->setAmount(20.24);

        $this->assertEquals(20.24, $currency->getAmount());
    }

    /**
     * @expectedException \App\Exceptions\CurrencyException
     */
    public function testSetAmountToThrowExceptionWithNegative()
    {
        $currency = new Currency('PLN');
        $currency->setAmount(-10);
    }

    /**
     * @expectedException \App\Exceptions\CurrencyException
     */
    public function testSetAmountToThrowExceptionWithZero()
    {
        $currency = new Currency('PLN');
        $currency->setAmount(0);
    }

    /**
     * @expectedException \App\Exceptions\CurrencyException
     */
    public function testSetAmountToThrowExceptionWithString()
    {
        $currency = new Currency('PLN');
        $currency->setAmount('asd');
    }

    /**
     * @expectedException \App\Exceptions\CurrencyException
     */
    public function testNotExistingCurrency()
    {
        $currency = new Currency('ASD');
    }

    /**
     * @expectedException \App\Exceptions\CurrencyException
     */
    public function testConvertToThrowExceptionWithInvalidCurrency()
    {
        $currency = new Currency('PLN');
        $currency->convertTo('ASD');
    }

    public function testConvertToWithoutDate()
    {
        $mock = Mockery::mock(\App\Interfaces\CurrencyConverterInterface::class);
        $mock->shouldReceive('from', 'to')
            ->once()
            ->andReturnSelf();
        $mock->shouldReceive('convert')
            ->once()
            ->andReturnNull();
        $mock->shouldReceive('hasErrors')
            ->once()
            ->andReturnFalse();
        $mock->shouldReceive('getTo')
            ->once()
            ->andReturn(new Currency('EUR', 4.5));
        
        $currency = new Currency('PLN', 1.0);
        $convertedCurrency = $currency->convert($mock, 'USD');
        
        $this->assertEquals(4.5, $convertedCurrency->getAmount());
    }

    public function testConvertToWithDate()
    {
        $mock = Mockery::mock(\App\Interfaces\CurrencyConverterInterface::class);
        $mock->shouldReceive('from', 'to', 'setDate')
            ->once()
            ->andReturnSelf();
        $mock->shouldReceive('convert')
            ->once()
            ->andReturnNull();
        $mock->shouldReceive('hasErrors')
            ->once()
            ->andReturnFalse();
        $mock->shouldReceive('getTo')
            ->once()
            ->andReturn(new Currency('EUR', 4.5));
        
        $currency = new Currency('PLN', 1.0);
        $convertedCurrency = $currency->convert($mock, 'USD', date('Y-m-d'));
        
        $this->assertEquals(4.5, $convertedCurrency->getAmount());
    }

    /**
     * @expectedException \App\Exceptions\CurrencyException
     */
    public function testConvertToThrowException()
    {
        $mock = Mockery::mock(\App\Interfaces\CurrencyConverterInterface::class);
        $mock->shouldReceive('from', 'to')
            ->once()
            ->andReturnSelf();
        $mock->shouldReceive('convert')
            ->once()
            ->andReturnNull();
        $mock->shouldReceive('hasErrors')
            ->once()
            ->andReturnTrue();
        $mock->shouldReceive('getErrorMessage')
            ->once()
            ->andReturn('Test');
        
        $currency = new Currency('PLN', 1.0);
        $currency->convert($mock, 'USD');
    }
    
    public function testIfCurrencyRounds()
    {
        $currency = new Currency('PLN', 12.56235);
        
        $this->assertEquals(12.57, $currency->getAmount());
    }
    
    public function testCurrencyConstructorRoundAlwaysUp()
    {
        $currency = new Currency('PLN', 12.56235, App\Enums\RoundMethodEnum::ALWAYS_UP);
        $this->assertEquals(12.57, $currency->getAmount());
    }

    public function testCurrencyConstructorRoundAlwaysDown()
    {
        $currency = new Currency('PLN', 12.56235, App\Enums\RoundMethodEnum::ALWAYS_DOWN);
        $this->assertEquals(12.56, $currency->getAmount());
    }
    
}
