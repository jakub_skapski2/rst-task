<?php

class ExchangeRateTest extends TestCase
{
    private $api;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->api = new App\Webservices\CurrencyConverter\ExchangeRate;
    }
    
    public function testGetRateByDateIsSuccessful()
    {
        $result = $this->api->getRateByDate('EUR', 'PLN', '2018-11-23');
        
        $this->assertEquals(true, is_float($result));
        
        $this->assertGreaterThan(4, $result);
    }
    
    /**
     * @expectedException GuzzleHttp\Exception\ClientException
     */
    public function testWrongCurrencyException()
    {
        $this->api->getRateByDate('asd', 'PLN', '2018-11-23');
    }
    
    /**
     * @expectedException App\Exceptions\RequestException
     */
    public function testNotAvailableDate()
    {
        $this->api->getRateByDate('EUR', 'PLN', '2019-11-23');
    }
    
    public function tearDown()
    {
        $this->api = null;
        
        parent::tearDown();
    }
}
