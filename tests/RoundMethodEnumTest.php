<?php

use App\Enums\RoundMethodEnum;

class RoundMethodEnumTest extends TestCase
{

    public function testToSelect()
    {
        foreach(RoundMethodEnum::toSelect() as $key => $value)
        {
            $this->assertTrue(is_int($key));
            $this->assertTrue(is_string($value));
        }
    }
    
    public function testIfUpIsDefault()
    {
        $array = RoundMethodEnum::toSelect();
        $first = reset($array);
        
        $this->assertEquals('ALWAYS_UP', $first);
    }
}
