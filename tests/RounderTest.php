<?php

use App\Enums\RoundMethodEnum;
use App\Classes\Rounder;

class RounderTest extends TestCase
{

    public function testIfAlwaysRoundsUp()
    {
        $this->assertEquals(6, Rounder::round(5.01, RoundMethodEnum::ALWAYS_UP, 0));
        $this->assertEquals(6, Rounder::round(5.99, RoundMethodEnum::ALWAYS_UP, 0));
        $this->assertEquals(6, Rounder::round(5.5, RoundMethodEnum::ALWAYS_UP, 0));
        $this->assertEquals(6, Rounder::round(6, RoundMethodEnum::ALWAYS_UP, 0));
    }

    public function testIfAlwaysRoundsDown()
    {
        $this->assertEquals(6, Rounder::round(6.01, RoundMethodEnum::ALWAYS_DOWN, 0));
        $this->assertEquals(6, Rounder::round(6.99, RoundMethodEnum::ALWAYS_DOWN, 0));
        $this->assertEquals(6, Rounder::round(6.5, RoundMethodEnum::ALWAYS_DOWN, 0));
        $this->assertEquals(6, Rounder::round(6, RoundMethodEnum::ALWAYS_DOWN, 0));
    }
    
    public function testDefaultMethod()
    {
        
        $this->assertEquals(6, Rounder::round(5.01, 'asd', 0));
        $this->assertEquals(6, Rounder::round(5.99, 'asd', 0));
        $this->assertEquals(6, Rounder::round(5.5, 'asd', 0));
        $this->assertEquals(6, Rounder::round(6, 'asd', 0));
        
        $this->assertEquals(6, Rounder::round(5.01));
        $this->assertEquals(6, Rounder::round(5.99));
        $this->assertEquals(6, Rounder::round(5.5));
        $this->assertEquals(6, Rounder::round(6));
    }
    
    /**
     * @expectedException App\Exceptions\RounderException
     */
    public function testToThrowExceptionWithValueString()
    {
        Rounder::round('asd', RoundMethodEnum::ALWAYS_UP, 0);
    }
    
    /**
     * @expectedException App\Exceptions\RounderException
     */
    public function testToThrowExceptionWithValueNull()
    {
        Rounder::round(null, RoundMethodEnum::ALWAYS_UP, 0);
    }
    
    /**
     * @expectedException App\Exceptions\RounderException
     */
    public function testToThrowExceptionWithPrecisionString()
    {
        Rounder::round(1, RoundMethodEnum::ALWAYS_UP, 'asd');
    }
    
    /**
     * @expectedException App\Exceptions\RounderException
     */
    public function testToThrowExceptionWithPrecisionNull()
    {
        Rounder::round(2, RoundMethodEnum::ALWAYS_UP, null);
    }
}
