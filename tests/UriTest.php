<?php

use App\Classes\Uri;

class UriTest extends TestCase
{

    public function testScheme()
    {
        $this->assertEquals('http', (new Uri('http://google.pl'))->getScheme());
        $this->assertEquals('https', (new Uri('https://google.pl'))->getScheme());
        $this->assertEquals('https', (new Uri('google.pl'))->getScheme());
    }

    public function testHost()
    {
        $this->assertEquals('google.pl', (new Uri('http://google.pl'))->getHost());
    }

    public function testPath()
    {
        $this->assertEquals('/asd/', (new Uri('http://google.pl/asd/'))->getPath());
        $this->assertEquals('/asd/', (new Uri('http://google.pl/asd'))->getPath());
        $this->assertEquals('/', (new Uri('http://google.pl/'))->getPath());
        $this->assertEquals('/', (new Uri('http://google.pl'))->getPath());
    }

    public function testSetPath()
    {
        $this->assertEquals('/asd/', (new Uri('http://google.pl/asd/'))->setPath('/asd/')->getPath());
        $this->assertEquals('/asd/', (new Uri('http://google.pl/asd'))->setPath('/asd')->getPath());
        $this->assertEquals('/', (new Uri('http://google.pl/'))->setPath('/')->getPath());
        $this->assertEquals('/', (new Uri('http://google.pl'))->setPath('')->getPath());
    }

    public function testQuery()
    {
        $this->assertEquals('?asd=test', (new Uri('http://google.pl/asd/?asd=test'))->getQuery());
        $this->assertEquals('?asd=test', (new Uri('http://google.pl/asd?asd=test'))->getQuery());
        $this->assertEquals('?asd=test', (new Uri('http://google.pl/?asd=test'))->getQuery());
        $this->assertEquals('?asd=test', (new Uri('http://google.pl?asd=test'))->getQuery());
        $this->assertNull((new Uri('http://google.plasd=test'))->getQuery());
    }

    public function testToString()
    {
        $this->assertEquals('http://google.pl/asd/', (new Uri('http://google.pl/asd/'))->setPath('/asd/')->toString());
        $this->assertEquals('http://google.pl/asd/', (new Uri('http://google.pl/asd'))->setPath('/asd')->toString());
        $this->assertEquals('http://google.pl/', (new Uri('http://google.pl/'))->setPath('/')->toString());
        $this->assertEquals('http://google.pl/', (new Uri('http://google.pl'))->setPath('')->toString());
    }
}
